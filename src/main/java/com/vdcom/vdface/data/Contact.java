package com.vdcom.vdface.data;

import com.vdcom.vdface.utils.EmailScheduler;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.logging.Logger;

import static java.time.temporal.ChronoUnit.DAYS;

public class Contact {

    private static Logger log = Logger.getLogger(EmailScheduler.class.getName());

    public String ContactId;
    public String Name;
    public String Phone;
    public String MobilePhone;
    public String MobilePhone2;
    public String Skype;
    public String Email;
    public String BirthDate;
    public String Gender;
    public String Account;
    public String Department;
    public String Photo;
    public String HeadName;
    public String JobName;
    public String MimeType;
    private LocalDate currentYearBirthdate;

    public LocalDate getCurrentYearBirthdate() {
        return currentYearBirthdate;
    }

    public void setCurrentYearBirthDate(LocalDate searchDate) {
        LocalDate bd = getBirthDates().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        currentYearBirthdate = LocalDate.of(searchDate.getYear(), bd.getMonth(), bd.getDayOfMonth());

    }

    public String getFormattedCurentYearBirthdate() {
        return new SimpleDateFormat("dd.MM, EEEE", new Locale("ru")).format(Date.from(currentYearBirthdate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
    }

    public void setPhoto(String Photo) {
        this.Photo = Photo;
    }

    public String getContactId() {
        return ContactId;
    }

    public String getName() {
        return Name;
    }

    public String getPhone() {
        return Phone;
    }

    public String getHeadName() {
        return HeadName;
    }

    public String getJobName() {
        return JobName;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public String getMobilePhone2() {
        return MobilePhone2;
    }

    public String getSkype() {
        return Skype;
    }

    public String getEmail() {
        return Email;
    }

    public Date getBirthDate() {

        return getBirthDates();
    }

    public LocalDate getBirthLocalDate() {
        Date birthDate = getBirthDate();

        return (birthDate == null) ? null : birthDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public long getDaysBetweenDate(LocalDate searchDate) {
        return DAYS.between(searchDate, LocalDate.of(searchDate.getYear(), this.getBirthLocalDate().getMonth(), this.getBirthLocalDate().getDayOfMonth()));
    }

    public String getBirthDayWithDoW() {
        DateTimeFormatter dt = DateTimeFormatter.ofPattern("dd.MM");

        Locale forLangLocale = Locale.forLanguageTag("ru-RU");

        return getBirthLocalDate().format(dt) + ", " + getBirthLocalDate().getDayOfWeek().getDisplayName(TextStyle.FULL, forLangLocale);

    }

    public Date getBirthDates() {
        ///Date(682113600000+0400)/

        String timeStr = BirthDate;//BirthDate.replaceAll(".*Date\\(", "").replaceAll("\\+.*", "");
        Date resDate = null;

        if (timeStr != null) {
            if (timeStr.matches(".*Date(.*\\+.*)")) {
                timeStr = timeStr.substring(timeStr.indexOf('(') + 1, timeStr.indexOf(')'));
                String[] timeparts = timeStr.split("\\+");
                resDate = Date.from(Instant.ofEpochMilli(Long.parseLong(timeparts[0])).atOffset(ZoneOffset.ofHours(Integer.parseInt(timeparts[1]) / 100)).toLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant());
            } else if (timeStr.matches(".*Date(.*)")) {
                timeStr = timeStr.substring(timeStr.indexOf('(') + 1, timeStr.indexOf(')'));
                resDate = Date.from(Instant.ofEpochMilli(Long.parseLong(timeStr)).atOffset(OffsetDateTime.now().getOffset()).toLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant());
            }
        }

        return resDate;
    }

    public String getFormattedBirthDate() {
        return new SimpleDateFormat("dd.MM, EEEE").format(getBirthDates());
    }

    public String getGender() {
        return Gender;
    }

    public String getAccount() {
        return Account;
    }

    public String getDepartment() {
        return (Department.equals("")) ? "-" : Department;
    }

    public String getPhoto() {
        return Photo;
    }

    public String getMimeType() {
        return MimeType;
    }

    public Boolean hasPhoto() {
        return Boolean.TRUE;
    }

    public String getPhones() {
        return "Внутренний: " + Phone + "<br>"
                + "Мобильный: " + MobilePhone + "<br>"
                + "Мобильный: " + MobilePhone2;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.ContactId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contact other = (Contact) obj;
        if (!Objects.equals(this.ContactId, other.ContactId)) {
            return false;
        }
        return true;
    }
}
