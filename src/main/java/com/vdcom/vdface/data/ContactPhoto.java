package com.vdcom.vdface.data;

public class ContactPhoto {

    public String Photo;
    public String MimeType;

    public String getPhoto() {
        return Photo;
    }

    public String getMimeType() {
        return MimeType;
    }
}
