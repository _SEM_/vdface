package com.vdcom.vdface.service;

import com.vdcom.vdface.data.Contact;
import com.vdcom.vdface.utils.ContactsLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContactService implements IContactService {

    private static final Set<Contact> contacts;
    private static boolean isLoading = false;
    private static Date loadDate = null;
    private String loginURL;
    private String dataURL;
    private String username;
    private String password;
    private String credentials;


    @Autowired
    private ContactsLoader loader;

    public void setLoader(ContactsLoader loader) {
        this.loader = loader;
    }

    private String account = "";

    static {
        contacts = new HashSet<>();
    }

    @Override
    public List<Contact> loadContact() {

        try {

            List<Contact> loaded = loader.load();
            contacts.clear();
            if (!(loaded == null)) {
                contacts.addAll(loaded);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new ArrayList<>(contacts);
    }

    @Override
    public List<String> getAccounts() {
        List<String> accounts = new ArrayList<String>();
        for (Contact c : contacts) {
            if (!accounts.contains(c.Account)) {
                accounts.add(c.Account);
            }
        }
        return accounts;
    }

    public List<String> getDepartments() {
        List<String> departments = new ArrayList<String>();
        for (Contact c : contacts) {
            if (!departments.contains(c.Department) && (c.Account.equals(account) || account == null || account.equals(""))) {
                departments.add(c.Department);
            }
        }
        return departments;
    }

    public List<String> getGenders() {
        List<String> genders = new ArrayList<String>();
        for (Contact c : contacts) {
            if (!genders.contains(c.Gender)) {
                genders.add(c.Gender);
            }
        }
        return genders;
    }

    public Contact getContactById(String id) {
        for (Contact c : contacts) {
            if (c.getContactId().equals(id)) {
                return c;
            }
        }
        return null;
    }

    public Map.Entry<String, String> getPhoto(String id) {
        for (Contact c : contacts) {
            if (c.getContactId().equals(id)) {
                return new AbstractMap.SimpleEntry<String, String>(c.MimeType, c.Photo);
            }
        }
        return new AbstractMap.SimpleEntry<String, String>("image/jpeg", "");
    }

    public String getAccount() {
        return account;
    }

    @Override
    public void setAccount(String account) {
        this.account = account;
    }


}
