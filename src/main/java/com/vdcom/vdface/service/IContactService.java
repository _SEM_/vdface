package com.vdcom.vdface.service;

import com.vdcom.vdface.data.Contact;

import java.util.List;

public interface IContactService {
    List<Contact> loadContact();

    void setAccount(String account);
    List<String> getAccounts();
}
