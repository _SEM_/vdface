package com.vdcom.vdface.service;

import com.vdcom.vdface.data.ContactPhoto;
import com.vdcom.vdface.utils.ContactsLoader;
import org.apache.poi.util.IOUtils;
import org.primefaces.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


@WebServlet("/ContactImageService")
public class ContactImageService extends HttpServlet {

    @Autowired
    private ContactsLoader loader;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    public void setLoader(ContactsLoader loader) {
        this.loader = loader;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String id = request.getParameter("id");

            ContactPhoto c = loader.loadPhoto(id);

            if (c != null && c.Photo != null && !c.Photo.trim().equals("")) {
                response.setContentType(c.MimeType);
                try (OutputStream out = response.getOutputStream()) {

                    byte[] imageBytes;
                    imageBytes = Base64.decode(c.Photo);
                    out.write(imageBytes);

                } catch (Exception e) {
                    System.out.println("com.vdcom.vdface.service.ContactImageService.processRequest()");
                    System.out.println(id);
                    e.printStackTrace();
                }
            } else {
                response.setContentType("image/jpg");
                try (InputStream stream = getServletContext().getResourceAsStream("/resources/images/noimage.jpg"); OutputStream out = response.getOutputStream()) {
                    byte[] imageBytes = IOUtils.toByteArray(stream);
                    out.write(imageBytes);
                } catch (Exception e) {

                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    public String getServletInfo() {
        return "Employee image service.";
    }
}
