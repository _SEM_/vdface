package com.vdcom.vdface.utils;


import com.vdcom.vdface.data.Contact;
import org.springframework.stereotype.Component;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.*;
import java.util.Base64;
import java.util.List;
import java.util.logging.Logger;


@Component
public class EmailSender {
    private static final Logger log = Logger.getLogger(EmailSender.class.getName());

    public void setMailSession(Session mailSession) {
        this.mailSession = mailSession;
    }

    private static String APP_PATH = System.getProperties().getProperty("catalina.home") + "/applications/vdface";
    private static String RESOURCE_PATH = "/resources/images/";
    private static String VDCOM_LOGO_FN = "VDcom.jpg";
    private static String SIXTY_NAMES_LOGO_FN = "60names.jpg";


    //TODO исправить ресурс
    //@Resource(name = "mail.vdcom.ru")
    @Resource(name = "mailSession")
    private Session mailSession;


    public EmailSender() {
    }

    private void addImageContent(MimeMultipart msgContent, byte[] imgBytes, String contentID, String fileName) throws MessagingException {
        MimeBodyPart imagePart = new MimeBodyPart();
        DataSource ds = new ByteArrayDataSource(imgBytes, "image/*");
        imagePart.setDataHandler(new DataHandler(ds));
        imagePart.setFileName(fileName);
        imagePart.setContentID(contentID);
        imagePart.setDisposition(MimeBodyPart.INLINE);
        msgContent.addBodyPart(imagePart);

    }


    public void SendMail(List<String> recipients, String body, String subject, List<Contact> contacts) throws MessagingException, UnsupportedEncodingException {
        MimeMessage msg = new MimeMessage(mailSession);

        for (String recp : recipients) {
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(recp));
        }
        msg.setFrom(new InternetAddress("inform@vdcom.ru", "Информационная служба VDсom"));
        msg.setHeader("Content-Type", "text/html; charset=UTF-8");
        msg.setSubject(subject, "UTF-8");

        MimeMultipart msgContent = new MimeMultipart("related");

        MimeBodyPart mainPart = new MimeBodyPart();
        mainPart.setText(body, "UTF-8", "html");

        msgContent.addBodyPart(mainPart);

        try {

            InputStream bis = new FileInputStream(new File(APP_PATH + RESOURCE_PATH + VDCOM_LOGO_FN));

            byte[] logo_byte = new byte[bis.available()];
            bis.read(logo_byte);

            addImageContent(msgContent, logo_byte, "<vdcom_logo>", VDCOM_LOGO_FN);

            bis = new FileInputStream(new File(APP_PATH + RESOURCE_PATH + SIXTY_NAMES_LOGO_FN));
            logo_byte = new byte[bis.available()];
            bis.read(logo_byte);
            addImageContent(msgContent, logo_byte, "<60names_logo>", SIXTY_NAMES_LOGO_FN);

        } catch (IOException e) {
            Logger.getLogger(EmailSender.class.getName()).warning("Can't load logo");
        }

        for (Contact c : contacts) {
            String photo = (c == null) ? null : c.getPhoto();

            if (photo != null && !photo.isEmpty()) {
                byte[] imgBytes = Base64.getMimeDecoder().decode(photo);
                addImageContent(msgContent,imgBytes,"<" + c.getContactId() + ">", c.getName().replaceAll("\\s+", "") + ".jpg");
            }
        }

        msg.setContent(msgContent);

        Transport.send(msg);
    }
}
