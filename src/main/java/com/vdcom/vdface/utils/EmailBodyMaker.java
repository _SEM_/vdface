/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vdcom.vdface.utils;

import com.vdcom.vdface.data.Contact;
import freemarker.template.TemplateException;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author SavelyevEM
 */
public interface EmailBodyMaker {
        

    String MakeEmailBody(List<Contact> contacts, File templatePath, LocalDate startDate, int offset, String baseURL) throws IOException, TemplateException;

}
