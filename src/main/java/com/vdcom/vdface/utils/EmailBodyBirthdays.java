/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vdcom.vdface.utils;

import com.vdcom.vdface.data.Contact;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



@Component
public class EmailBodyBirthdays implements EmailBodyMaker {

    @Override
    public String MakeEmailBody(List<Contact> contacts, File templatePath, LocalDate startDate, int offset, String baseURL) throws IOException, TemplateException {

        Configuration cfg = new Configuration(Configuration.VERSION_2_3_27);

        cfg.setDirectoryForTemplateLoading(templatePath.getParentFile());
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setLogTemplateExceptions(false);
        cfg.setWrapUncheckedExceptions(true);

        Map<String, Object> root = new HashMap<>();
        root.put("contacts", contacts);

        DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM");
        root.put("startDate", startDate.format(df));
        root.put("endDate", startDate.plusDays(offset).format(df));
        root.put("baseURL", baseURL);

        Template temp = cfg.getTemplate(templatePath.getName());
        StringWriter out = new StringWriter();
        temp.process(root, out);


        return out.getBuffer().toString();
    }

}
