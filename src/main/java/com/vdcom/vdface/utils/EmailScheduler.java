package com.vdcom.vdface.utils;

import com.vdcom.vdface.data.Contact;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.stream.Collectors.toList;


@EnableScheduling
@Component
public class EmailScheduler implements IEmailScheduler {


    private static Logger log = Logger.getLogger(EmailScheduler.class.getName());

    @Autowired
    private EmailSender eSender;

    @Autowired
    private EmailBodyBirthdays ebb;

    @Autowired
    private AppSettings config;


    @Autowired
    private ContactsLoader contactsLoader;

    public void setContactsLoader(ContactsLoader contactsLoader) {
        this.contactsLoader = contactsLoader;
    }

    public void setConfig(AppSettings config) {
        this.config = config;
    }

    public void seteSender(EmailSender eSender) {
        this.eSender = eSender;
    }

    public void setEbb(EmailBodyBirthdays ebb) {
        this.ebb = ebb;
    }

    private List<Contact> contacts;
    private LocalDate searchDate;
    private String messageBody;
    private int offset = 13;

    @PostConstruct
    private void init() {

        if (config != null) {
            log.info(config.getParamValue(AppSettings.TEMPLATE_PATH));
        } else log.log(Level.WARNING, "can't find template");

    }

    @Scheduled(cron = "0 0 8 ? * MON")
    //@Scheduled(fixedDelay = 1000000)
    @Override
    public void SendBirthdayLetters() {

        if (contactsLoader != null) {
            contacts = contactsLoader.load();
            searchDate = LocalDate.now();
            contacts = contacts.stream().filter(c -> c.getDaysBetweenDate(searchDate) <= 14 && c.getDaysBetweenDate(searchDate) >= 0).collect(toList());
        }


        if (contacts != null) {

            contacts.forEach(e -> e.setPhoto(contactsLoader.loadPhoto(e.getContactId()).getPhoto()));
            contacts.forEach(e -> e.setCurrentYearBirthDate(searchDate));

            contacts.sort(new Comparator<Contact>() {
                @Override
                public int compare(Contact o1, Contact o2) {
                    return o1.getCurrentYearBirthdate().compareTo(o2.getCurrentYearBirthdate());
                }
            });
            File file = new File(config.getParamValue(AppSettings.TEMPLATE_PATH));

            try {
                messageBody = ebb.MakeEmailBody(contacts, file, searchDate, offset, "");

                List<String> recieps = new ArrayList<>();
                recieps.addAll(Arrays.asList(config.getParamValue(AppSettings.RECIPIENTS).split(";")));

                eSender.SendMail(recieps, messageBody, "Дни рождения!", contacts);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TemplateException e) {
                e.printStackTrace();
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
    }
}
