package com.vdcom.vdface.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vdcom.vdface.data.Contact;
import com.vdcom.vdface.data.ContactPhoto;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class ContactsLoader {

    @Autowired
    private AppSettings config;

    private String loginURL;
    private String dataURL;
    private String photoURL;
    private String secCred;
    private Cookie aCookie;

    public void setConfig(AppSettings config) {
        this.config = config;
    }

    public ContactsLoader() {

    }

    @PostConstruct
    private void init() {
        String username = "", password = "", credentials = "";
        dataURL = config.getParamValue(AppSettings.DATA_URL);
        loginURL = config.getParamValue(AppSettings.LOGIN_URL);
        username = config.getParamValue(AppSettings.USERNAME);
        password = config.getParamValue(AppSettings.PASSWORD);
        credentials = config.getParamValue(AppSettings.CREDENTIALS);
        photoURL = config.getParamValue(AppSettings.PHOTO_URL);
        secCred = String.format(credentials, username, password);
    }

    public ContactsLoader(String loginURL, String dataURL, String secCred) {
        this.loginURL = loginURL;
        this.dataURL = dataURL;
        this.secCred = secCred;
    }

    public List<Contact> load() {
        List<Contact> result = null;
        try {
            ObjectMapper nobm = new ObjectMapper();

            String dataStr = getData();
            if (dataStr.contains("[") | dataStr.contains("{")) {
                dataStr = StringEscapeUtils.unescapeJson(((dataStr.contains("["))
                        ? dataStr.substring(dataStr.indexOf("["))
                        : dataStr.substring(dataStr.indexOf("{"))).replace("\"</string>", ""));
                result = nobm.readValue(dataStr, new TypeReference<List<Contact>>() {
                });
            }
        } catch (IOException ex) {
            Logger.getLogger(ContactsLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public ContactPhoto loadPhoto(String id) {
        ContactPhoto result = null;
        String res = "";
        try {
            ObjectMapper nobm = new ObjectMapper();

            String dataStr = getPhotoData(id);
            if (dataStr.contains("[") | dataStr.contains("{")) {
                res = "";
                dataStr = StringEscapeUtils.unescapeJson(((dataStr.contains("["))
                        ? dataStr.substring(dataStr.indexOf("["))
                        : dataStr.substring(dataStr.indexOf("{"))).replace("\"</string>", ""));
                res = dataStr;
                result = nobm.readValue(dataStr, new TypeReference<ContactPhoto>() {
                });
            }
        } catch (IOException ex) {
            Logger.getLogger(ContactsLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            Logger.getLogger(ContactsLoader.class.getName()).log(Level.SEVERE, null, e);
        }
        return result;
    }

    private String getData() {
        getAuthCookie();
        HttpGet get_req = new HttpGet(dataURL);
        BasicCookieStore cs = new BasicCookieStore();
        cs.addCookie(aCookie);
        StringBuilder sb = new StringBuilder();
        String line;

        HttpClient cl = HttpClientBuilder.create().setDefaultCookieStore(cs).build();
        try {
            HttpResponse resp = cl.execute(get_req);
            BufferedReader reader = new BufferedReader(new InputStreamReader(resp.getEntity().getContent(), "utf-8"));
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private String getPhotoData(String id) {
        getAuthCookie();

        BasicCookieStore cs = new BasicCookieStore();
        cs.addCookie(aCookie);
        StringBuilder sb = new StringBuilder();
        String line;

        HttpClient cl = HttpClientBuilder.create().setDefaultCookieStore(cs).build();
        try {
            URIBuilder uri = new URIBuilder(photoURL);

            uri.setParameter("paramContactId", id);
            uri.setParameter("paramPreview", "true");
            uri.setParameter("ResultParameterName", "paramPhoto");

            HttpGet get_req = new HttpGet(uri.build());

            HttpResponse resp = cl.execute(get_req);
            BufferedReader reader = new BufferedReader(new InputStreamReader(resp.getEntity().getContent(), "utf-8"));
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private void getAuthCookie() {
        HttpClient cl = HttpClientBuilder.create().build();
        HttpPost post_req = new HttpPost(loginURL);
        post_req.setHeader("Content-Type", MediaType.APPLICATION_JSON);
        post_req.setHeader("ForceUseSession", "true");
        HttpClientContext httpClientContext = new HttpClientContext();
        HttpEntity sec = new StringEntity(secCred, "utf-8");

        try {
            post_req.setEntity(sec);
            HttpResponse resp = cl.execute(post_req, httpClientContext);
            for (Cookie nc : httpClientContext.getCookieStore().getCookies()) {
                if (nc.getName().equals(".ASPXAUTH")) {
                    aCookie = nc;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
