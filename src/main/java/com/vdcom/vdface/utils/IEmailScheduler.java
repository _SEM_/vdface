package com.vdcom.vdface.utils;

import org.springframework.scheduling.annotation.Scheduled;

public interface IEmailScheduler {
    @Scheduled(cron = "0 0 8 ? * MON")
    void SendBirthdayLetters();
}
