package com.vdcom.vdface.utils;


import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class AppSettings {
    private static final Logger log = Logger.getLogger(AppSettings.class.getName());

    public static final String DATA_URL = "dataurl";
    public static final String LOGIN_URL = "loginurl";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String CREDENTIALS = "credentials";
    public static final String PHOTO_URL = "photourl";
    public static final String TEMPLATE_PATH = "templatepath";
    public static final String RECIPIENTS = "recipients";

    private Properties props;

    public String getParamValue(String paramName) {
        String value = "";
        Boolean jndiParamsNotFilled=false;

        try {
            InitialContext ic = new InitialContext();
            value = (String) ic.lookup("vdface/"+paramName);
        } catch (NameNotFoundException e) {
            jndiParamsNotFilled = true;
        } catch (NamingException ne) {
            jndiParamsNotFilled = true;
        }

        if (jndiParamsNotFilled) {
            value = props.get("service."+paramName).toString();
        }

        return value;
    }

    @PostConstruct
    public void init() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        InputStream propstream = classLoader.getResourceAsStream("config.properties");
        props = new Properties();

        try {
            props.load(propstream);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

    }
}
