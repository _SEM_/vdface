/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vdcom.vdface.view;

import com.vdcom.vdface.data.Contact;
import com.vdcom.vdface.service.ContactService;
import com.vdcom.vdface.service.IContactService;
import com.vdcom.vdface.utils.EmailScheduler;
import com.vdcom.vdface.utils.IEmailScheduler;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * @author SavelyevEM
 */
@ManagedBean(name = "dtContactView")
@ViewScoped
public class ContactsView implements Serializable {

    private List<Contact> contacts;

    private List<Contact> filteredContacts;

    private String account = "";

    @ManagedProperty(value = "#{contactService}")
    private ContactService service;

    @ManagedProperty(value = "#{emailScheduler}")
    private EmailScheduler es;

    @PostConstruct
    public void init() {
        contacts = service.loadContact();
        //es.SendBirthdayLetters();
    }

    public List<String> getAccounts() {
        return service.getAccounts();
    }

    public List<Contact> getContacts() {
        contacts = service.loadContact();

        contacts.sort(new Comparator<Contact>() {
            @Override
            public int compare(Contact o1, Contact o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
        return contacts;
    }

    public List<Contact> getFilteredContacts() {
        return filteredContacts;
    }

    public void setFilteredContacts(List<Contact> filteredContacts) {
        this.filteredContacts = filteredContacts;
    }

    public List<Contact> getBirthdayBoy(Date birthday) {
        return contacts;
    }

    public void setService(ContactService service) {
        this.service = service;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void onAccountChange() {
        if (account != null && !account.equals("")) {
            service.setAccount(account);
        } else {
            service.setAccount("");
        }
    }

    public void sendEmails(ActionEvent actionEvent) {
        if (es == null) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "email sender not defined", null);
            FacesContext.getCurrentInstance().addMessage(null, message);
        } else {
            es.SendBirthdayLetters();
        }
    }

    public void setEs(EmailScheduler es) {
        this.es = es;
    }
}
