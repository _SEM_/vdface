package com.vdcom.vdface.data;

import org.junit.Assert;
import org.junit.Test;


import java.time.LocalDate;


public class ContactTest {

    @Test
    public void getFormattedCurentYearBirthdateTest() {
        String expectedString = "14.08, вторник";
        Contact contact = new Contact();
        contact.BirthDate = "Date(682113600000+0400)";
        contact.setCurrentYearBirthDate(LocalDate.of(2018,8,14));
        Assert.assertEquals(expectedString, contact.getFormattedCurentYearBirthdate());

    }
}