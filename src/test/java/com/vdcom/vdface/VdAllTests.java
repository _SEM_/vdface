package com.vdcom.vdface;

import com.vdcom.vdface.data.ContactTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ContactTest.class})
public class VdAllTests {

}
